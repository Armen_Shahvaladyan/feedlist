//
//  Gallery.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation
import CoreData

class PhotoItem: Codable {
    
    //MARK: - Properties
    let title: String
    let contentUrl: String
    let thumbnailUrl: String
    
    static let entityName = "Photo"
    
    //MARK: - Life Cycle
    required init(managedObject: NSManagedObject) {
        title        = managedObject.value(forKey: "title") as? String ?? ""
        contentUrl   = managedObject.value(forKey: "contentUrl") as? String ?? ""
        thumbnailUrl = managedObject.value(forKey: "thumbnailUrl") as? String ?? ""
    }
    
    //MARK: - Public API
    func setValuesTo(managedObject: NSManagedObject) {
        managedObject.setValue(title, forKey: "title")
        managedObject.setValue(contentUrl, forKey: "contentUrl")
        managedObject.setValue(thumbnailUrl, forKey: "thumbnailUrl")
    }
}
