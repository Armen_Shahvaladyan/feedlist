//
//  Video.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation
import CoreData

class VideoItem: Codable {
    
    //MARK: - Properties
    let title: String
    let thumbnailUrl: String
    var youtubeId: String
    
    static let entityName = "Video"
    
    //MARK: - Life Cycle
    required init(managedObject: NSManagedObject) {
        title        = managedObject.value(forKey: "title") as? String ?? ""
        thumbnailUrl = managedObject.value(forKey: "thumbnailUrl") as? String ?? ""
        youtubeId    = managedObject.value(forKey: "youtubeId") as? String ?? ""
    }
    
    //MARK: - Public API
    func setValuesTo(managedObject: NSManagedObject) {
        managedObject.setValue(title, forKey: "title")
        managedObject.setValue(thumbnailUrl, forKey: "thumbnailUrl")
        managedObject.setValue(youtubeId, forKey: "youtubeId")
    }
}
