//
//  Feed.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation
import CoreData

class Feed: Codable {
    
    //MARK: - Properties
    let date: Date
    let body: String
    let title: String
    let category: String
    let shareUrl: String
    let identifier: String
    let coverPhotoUrl: String
    var photoItems: [PhotoItem]?
    var videoItems: [VideoItem]?
    
    var isRead = false
    var isOpen = false
        
    static let entityName = "Feed"
    
    lazy var attributedDate: NSAttributedString = {
         FeedUIHelper.date(with: self)
    }()
    
    lazy var attributedBody: NSAttributedString = {
        FeedUIHelper.body(with: self)
    }()
    
    lazy var attributedTitle: NSAttributedString = {
        FeedUIHelper.title(with: self)
    }()
    
    lazy var attributedCategory: NSAttributedString = {
        FeedUIHelper.category(with: self)
    }()
    
    //MARK: - Life Cycle
    enum CodingKeys: String, CodingKey {
        case date, body, title, category, shareUrl, coverPhotoUrl
        case photoItems = "gallery"
        case videoItems = "video"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let interval  = try container.decode(Double.self, forKey: .date)
        date          = Date(timeIntervalSince1970: interval)
        body          = try container.decode(String.self, forKey: .body)
        title         = try container.decode(String.self, forKey: .title)
        category      = try container.decode(String.self, forKey: .category)
        shareUrl      = try container.decode(String.self, forKey: .shareUrl)
        coverPhotoUrl = try container.decode(String.self, forKey: .coverPhotoUrl)
        
        photoItems    = try? container.decode([PhotoItem].self, forKey: .photoItems)
        videoItems    = try? container.decode([VideoItem].self, forKey: .videoItems)
        
        identifier    = "\(date) \(title) \(category)"
    }
    
    required init(managedObject: NSManagedObject) {
        date          = managedObject.value(forKey: "date") as? Date ?? Date()
        body          = managedObject.value(forKey: "body") as? String ?? ""
        title         = managedObject.value(forKey: "title") as? String ?? ""
        isRead        = managedObject.value(forKey: "isRead") as? Bool ?? false
        category      = managedObject.value(forKey: "category") as? String ?? ""
        shareUrl      = managedObject.value(forKey: "shareUrl") as? String ?? ""
        coverPhotoUrl = managedObject.value(forKey: "coverPhotoUrl") as? String ?? ""
        identifier    = managedObject.value(forKey: "identifier") as? String ?? ""
        
        if let managedObjects = managedObject.mutableSetValue(forKey: "photoItems") as? Set<NSManagedObject> {
            var photoItems: [PhotoItem] = []
            for managedObject in managedObjects {
                photoItems.append(PhotoItem(managedObject: managedObject))
            }
            self.photoItems = photoItems
        }
        
        if let managedObjects = managedObject.mutableSetValue(forKey: "videoItems") as? Set<NSManagedObject> {
            var videoItems: [VideoItem] = []
            for managedObject in managedObjects {
                videoItems.append(VideoItem(managedObject: managedObject))
            }
            self.videoItems = videoItems
        }
    }
    
    //MARK: - Public API
    func setValuesTo(managedObject: NSManagedObject) {
        managedObject.setValue(date, forKey: "date")
        managedObject.setValue(body, forKey: "body")
        managedObject.setValue(title, forKey: "title")
        managedObject.setValue(isRead, forKey: "isRead")
        managedObject.setValue(category, forKey: "category")
        managedObject.setValue(shareUrl, forKey: "shareUrl")
        managedObject.setValue(coverPhotoUrl, forKey: "coverPhotoUrl")
        managedObject.setValue(identifier, forKey: "identifier")
    }
}

extension Feed: Equatable {
    static func == (lhs: Feed, rhs: Feed) -> Bool {
        lhs.identifier == rhs.identifier
    }
}

extension Feed: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
}

