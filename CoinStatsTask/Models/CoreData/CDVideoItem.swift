//
//  CDVideoItem.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation
import CoreData

class CDVideoItem: NSManagedObject {
    
    //MARK: - Properties
    @NSManaged var title: String?
    @NSManaged var thumbnailUrl: String?
    @NSManaged var youtubeId: String?
}
