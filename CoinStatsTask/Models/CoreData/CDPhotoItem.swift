//
//  CDPhotoItem.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation
import CoreData

class CDPhotoItem: NSManagedObject {
    
    //MARK: - Properties
    @NSManaged var title: String?
    @NSManaged var contentUrl: String?
    @NSManaged var thumbnailUrl: String?
}
