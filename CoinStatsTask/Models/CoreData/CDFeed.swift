//
//  CDFeed.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation
import CoreData

class CDFeed: NSManagedObject {
    
    //MARK: - Properties
    @NSManaged var isRead: Bool
    @NSManaged var date: Date?
    @NSManaged var body: String?
    @NSManaged var title: String?
    @NSManaged var category: String?
    @NSManaged var shareUrl: String?
    @NSManaged var identifier: String?
    @NSManaged var coverPhotoUrl: String?
    
    //MARK: - RelationShips
    @NSManaged var photoItems: Set<CDPhotoItem>?
    @NSManaged var videoItems: Set<CDVideoItem>?
}
