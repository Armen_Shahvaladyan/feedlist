//
//  DatabaseService.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation
import CoreData

protocol DatabaseServiceProtocol {
    func load(completionHandler: @escaping (Error?) -> Void)
    func fetchFeeds(completion: @escaping (Result<[Feed], FeedsError>) -> Void)
    func syncFeeds(_ feeds: [Feed], completion: @escaping (FeedsError?) -> Void)
    func updateFeeds(_ feeds: [Feed], completion: @escaping (FeedsError?) -> Void)
}

class DatabaseService: DatabaseServiceProtocol {
    
    //MARK: - Properties
    private var persistentContainer: NSPersistentContainer
    private var backgroundContext: NSManagedObjectContext?
    
    //MARK: - Life Cycle
    init(with url: URL?) {
        persistentContainer = NSPersistentContainer(name: "FeedData")
        if let url = url, url.path != persistentContainer.persistentStoreDescriptions.first?.url?.path {
            persistentContainer.persistentStoreDescriptions = [NSPersistentStoreDescription(url: url)]
        }
    }
    
    //MARK: - Public API
    func load(completionHandler: @escaping (Error?) -> Void) {
        persistentContainer.loadPersistentStores(completionHandler: { (_, error) in
            DispatchQueue.main.async {
                if error == nil {
                    self.backgroundContext = self.persistentContainer.newBackgroundContext()
                }
                completionHandler(error)
            }
        })
    }
    
    //MARK: - Private API
    private func commitTransaction(transactionBlock: @escaping (NSManagedObjectContext) -> Void, completionHandler: @escaping (Error?) -> Void) {
        if let backgroundContext = backgroundContext {
            backgroundContext.perform {
                var transactionError: Error?
                do {
                    transactionBlock(backgroundContext)
                    try backgroundContext.save()
                } catch {
                    transactionError = error
                }
                DispatchQueue.main.async {
                    completionHandler(transactionError)
                }
            }
        } else {
            completionHandler(nil)
        }
    }
    
    private func performReadObjects(block: @escaping (NSManagedObjectContext) -> [Any], completionHandler: @escaping ([Any]) -> Void) {
        if let backgroundContext = backgroundContext {
            backgroundContext.perform {
                let objects = block(backgroundContext)
                DispatchQueue.main.async {
                    completionHandler(objects)
                }
            }
        } else {
            completionHandler([Any]())
        }
    }
    
    private func fetchCDFeeds(predicate: NSPredicate? = nil,
                              limit: Int = 0,
                              entityName: String,
                              sortDescriptors: [NSSortDescriptor]? = nil,
                              inContext context: NSManagedObjectContext) -> [NSManagedObject]? {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        fetchRequest.predicate = predicate
        fetchRequest.fetchLimit = limit
        fetchRequest.sortDescriptors = sortDescriptors
        var objects: [NSManagedObject]?
        do {
            let result = try context.fetch(fetchRequest)
            objects = result
        } catch {}
        return objects
    }
    
    private func fetchIdentifiers(with feeds: [Feed]) -> [String] {
        feeds.map { $0.identifier }
    }
    
    private func update(managedObjects: [NSManagedObject], objects: [Feed]) {
        for managedObject in managedObjects {
            for object in objects {
                if object.identifier == (managedObject.value(forKey: "identifier") as? String) {
                    object.setValuesTo(managedObject: managedObject)
                }
            }
        }
    }
    
    //MARK: - DatabaseServiceProtocol
    func fetchFeeds(completion: @escaping (Result<[Feed], FeedsError>) -> Void) {
        performReadObjects(block: { (context: NSManagedObjectContext) -> [Any] in
            let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
            let localCDFeeds = self.fetchCDFeeds(entityName: Feed.entityName,
                                                 sortDescriptors: [sortDescriptor],
                                                 inContext: context) ?? []
            return localCDFeeds.map { Feed(managedObject: $0) }
        }, completionHandler: { (feeds: [Any]) in
            completion(.success((feeds as! [Feed]) ))
        })
    }
    
    func syncFeeds(_ feeds: [Feed], completion: @escaping (FeedsError?) -> Void) {
        commitTransaction(transactionBlock: { (context: NSManagedObjectContext) in
            let feedIds = self.fetchIdentifiers(with: feeds)
            let predicate = NSPredicate(format: "identifier in %@", feedIds)
            let localCDFeeds = self.fetchCDFeeds(predicate: predicate,
                                                 limit: feedIds.count,
                                                 entityName: Feed.entityName,
                                                 inContext: context)
            
            let localFeeds = localCDFeeds?.map { Feed(managedObject: $0) }
            
            var newFeeds = Set(feeds)
            let localFeedsSet = Set(localFeeds ?? [])
            newFeeds = newFeeds.subtracting(localFeedsSet)
            
            newFeeds.forEach { feed in
                let cdFeed = NSEntityDescription.insertNewObject(forEntityName: Feed.entityName, into: context)
                feed.setValuesTo(managedObject: cdFeed)
                let photoItemsRelationShip = cdFeed.mutableSetValue(forKey: "photoItems")
                for photo in feed.photoItems ?? [] {
                    let cdPhoto = NSEntityDescription.insertNewObject(forEntityName: PhotoItem.entityName, into: context)
                    photo.setValuesTo(managedObject: cdPhoto)
                    photoItemsRelationShip.add(cdPhoto)
                }
                let videoItemsRelationShip = cdFeed.mutableSetValue(forKey: "videoItems")
                for video in feed.videoItems ?? [] {
                    let cdVideo = NSEntityDescription.insertNewObject(forEntityName: VideoItem.entityName, into: context)
                    video.setValuesTo(managedObject: cdVideo)
                    videoItemsRelationShip.add(cdVideo)
                }
            }
        }, completionHandler: { error in
            if let error = error {
                completion(.unknown(error.localizedDescription))
            } else {
                completion(nil)
            }
        })
    }
    
    func updateFeeds(_ feeds: [Feed], completion: @escaping (FeedsError?) -> Void) {
        commitTransaction { context in
            let feedIds = self.fetchIdentifiers(with: feeds)
            let predicate = NSPredicate(format: "identifier in %@", feedIds)
            let localCDFeeds = self.fetchCDFeeds(predicate: predicate,
                                                 limit: feedIds.count,
                                                 entityName: Feed.entityName,
                                                 inContext: context) ?? []
            self.update(managedObjects: localCDFeeds, objects: feeds)
        } completionHandler: { error in
            completion(.unknown(error?.localizedDescription ?? ""))
        }
    }
}
