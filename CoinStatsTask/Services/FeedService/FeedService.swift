//
//  FeedService.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation

protocol FeedServiceProtocol {
    func syncFeeds(completion: @escaping (FeedsError?) -> Void)
    func fetchFeeds(completion: @escaping (Result<[Feed], FeedsError>) -> Void)
    func updateFeeds(_ feeds: [Feed], completion: @escaping (FeedsError?) -> Void)
}

class FeedService: FeedServiceProtocol {
    
    //MARK: - Properties
    private let networkService: NetworkServiceProtocol
    private let databaseService: DatabaseServiceProtocol
    
    //MARK: - Life Cycle
    init() {
        networkService = NetworkService()
        databaseService = DatabaseService(with: FileManager.feedDatabaseURL())
    }
    
    func loadDatabase(completionHandler: @escaping (Error?) -> Void) {
        databaseService.load(completionHandler: completionHandler)
    }
    
    //MARK: - FeedServiceProtocol
    func syncFeeds(completion: @escaping (FeedsError?) -> Void) {
        networkService.getFeeds { result in
            switch result {
            case .success(let feeds):
                self.databaseService.syncFeeds(feeds) { error in
                    if let error = error {
                        completion(.unknown(error.localizedDescription))
                    } else {
                        completion(nil)
                    }
                }
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func fetchFeeds(completion: @escaping (Result<[Feed], FeedsError>) -> Void) {
        databaseService.fetchFeeds(completion: completion)
    }
    
    func updateFeeds(_ feeds: [Feed], completion: @escaping (FeedsError?) -> Void) {
        databaseService.updateFeeds(feeds, completion: completion)
    }
}
