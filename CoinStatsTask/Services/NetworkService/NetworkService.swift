//
//  NetworkService.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation

protocol NetworkServiceProtocol {
    func getFeeds(completion: @escaping (Result<[Feed], FeedsError>) -> Void)
}

class NetworkService: NetworkServiceProtocol {
    
    //MARK: - Properties
    private let provider = Provider<FeedEndPoint>()
    
    //MARK: - NetworkServiceProtocol
    func getFeeds(completion: @escaping (Result<[Feed], FeedsError>) -> Void) {
        provider.request(target: .feeds,
                         type: [Feed].self) { feeds, error in
            if let error = error {
                completion(.failure(.unknown(error.message)))
            } else if let feeds = feeds {
                if feeds.isEmpty {
                    completion(.failure(.feedsEmpty))
                } else {
                    completion(.success(feeds))
                }
            }
        }
    }
}
