//
//  Provider.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import Moya

final class Provider<T>: MoyaProvider<T> where T: MultiTargetType {
    typealias Target = T
    
    init() {
        super.init(endpointClosure: MoyaProvider.defaultEndpointMapping, requestClosure: MoyaProvider<Target>.defaultRequestMapping, stubClosure: MoyaProvider.neverStub, session: NetworkConfiguration.sessionManager, plugins: [NetworkActivityPlugin(networkActivityClosure: NetworkConfiguration.networkActivityIndicator), NetworkConfiguration.networkLogger], trackInflights: false)
    }
    
    func request(target: Target,
                 callbackQueue: DispatchQueue? = .main,
                 completion: @escaping ([[String: Any]]?, CSError?) -> ()) {
        request(target, callbackQueue: callbackQueue) { result  in
            switch result {
            case .success(let response):
                do {
                    if let jsonObject = try JSONSerialization.jsonObject(with: response.data, options: .fragmentsAllowed) as? [String: Any] {
                        if let dataObject = jsonObject["data"] as? [[String: Any]] {
                            completion(dataObject, nil)
                        } else {
                            completion(nil, CSError(key: "parseIssue", message: "Parse issue"))
                        }
                    }
                } catch let error {
                    print(error.localizedDescription)
                    completion(nil, CSError(key: "parseIssue", message: error.localizedDescription))
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(nil, CSError(key: "", message: error.localizedDescription))
            }
        }
    }
    
    func request<C>(target: Target, type: C.Type?, path: ResponsePath? = nil, callbackQueue: DispatchQueue? = .main, completion: @escaping (C?, CSError?) -> ()) where C: Codable {
        request(target, callbackQueue: callbackQueue) { result  in
            switch result {
            case .success(let response):
                do {
                    let responseData = try response.mapObject(C.self, path: path?.rawValue)
                    if response.statusCode == 401 || response.statusCode == 403 {
                        //AppController.logout() TODO: Implement @Armen Shahvaladyan
                        completion(nil, nil)
                    } else {
                        if response.statusCode == 200, responseData.success {
                            if let data = responseData.data {
                                completion(data, nil)
                            } else {
                                completion(nil, CSError(key: "parseIssue", message: "Parse issue"))
                            }
                        } else {
                            completion(nil, responseData.errors?.first)
                        }
                    }
                } catch {
                    print(error.localizedDescription)
                    completion(nil, CSError(key: "parseIssue", message: "Parse issue"))
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion(nil, CSError(key: "", message: error.localizedDescription))
            }
        }
    }
    
    func request<C>(target: Target, type: [C].Type?, path: ResponsePath? = nil, callbackQueue: DispatchQueue? = .none, completion: @escaping ([C]?, CSError?) -> ()) where C: Codable {
        request(target, callbackQueue: callbackQueue) { result  in
            switch result {
            case .success(let response):
                do {
                    let responseData = try response.mapArray(C.self, path: path?.rawValue)
                    if response.statusCode == 401 || response.statusCode == 403 {
                        //AppController.logout() TODO: Implement @Armen Shahvaladyan
                        completion(nil, nil)
                    } else {
                        if response.statusCode == 200, responseData.success {
                            completion(responseData.data, nil)
                        } else {
                            completion(nil, responseData.errors?.first)
                        }
                    }
                } catch {
                    print(error.localizedDescription)
                    completion(nil, CSError(key: "", message: error.localizedDescription))
                }
                print(response.statusCode)
            case .failure(let error):
                print(error.localizedDescription)
                completion(nil, CSError(key: "", message: error.localizedDescription))
            }
        }
    }
}
