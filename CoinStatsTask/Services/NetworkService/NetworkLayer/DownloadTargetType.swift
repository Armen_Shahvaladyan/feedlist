//
//  DownloadTargetType.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import Moya

protocol DownloadTargetType: TargetType { }

extension DownloadTargetType {
    /// Destination for downloaded file
    var downloadDestination: DownloadDestination {
        get {
            return { temporaryURL, response in
                let fileURL = FileSystem.directory(.download).appendingPathComponent(response.suggestedFilename ?? "\(Date().timeIntervalSince1970)")
                return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
            }
        }
    }
}
