//
//  FeedEndPoint.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import Moya

enum FeedEndPoint {
    case feeds
}

extension FeedEndPoint: MultiTargetType {
    var path: String {
        switch self {
        case .feeds:
            return "feed"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        .requestPlain
    }
}
