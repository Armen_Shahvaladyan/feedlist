//
//  BaseResponse.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import Foundation

struct CSError: Codable {
    let key: String
    let message: String
}

struct BaseResponse<T>: Encodable, Decodable where T: Codable {
    var success: Bool
    var data: T?
    var errors: [CSError]?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        errors = try? container.decode([CSError].self, forKey: .errors)
        data = try? container.decode(T.self, forKey: .data)
        success = (try? container.decode(Bool.self, forKey: .success)) ?? false
    }
}

struct Empty: Codable {}
