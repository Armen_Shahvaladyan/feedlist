//
//  Environment.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import Foundation

enum Environment: String {
    case live
    case staging
    case development
    case branch

    var baseURLString: String {
        switch self {
        case .live: return "https://coinstats.getsandbox.com"
        case .staging: return "https://coinstats.getsandbox.com"
        case .development: return "https://coinstats.getsandbox.com"
        case .branch: return "https://coinstats.getsandbox.com"
        }
    }
    
    var baseURL: URL {
        return URL(string: self.baseURLString)!
    }

    var apiURLString: String {
        return "\(self.baseURL)/api"
    }
    
    var apiURL: URL {
        return URL(string: apiURLString)!
    }
}
