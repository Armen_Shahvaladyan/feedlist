//
//  NavigationController.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import UIKit

class NavigationController: UINavigationController {
    
    // MARK: - Lyfe Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.SF(.text, weight: .semibold, size: 17),
                                                         .foregroundColor: UIColor.appDarkOrange]
        
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = .white
        appearance.titleTextAttributes = attributes
        appearance.largeTitleTextAttributes = [.font: UIFont.SF(.text, weight: .bold, size: 34),
                                               .foregroundColor: UIColor.appDarkOrange]
        
        UINavigationBar.appearance().tintColor = .appDarkOrange
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        self.view.backgroundColor = .white
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: animated)
    }
}
