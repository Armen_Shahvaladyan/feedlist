//
//  FeedsError {.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation

enum FeedsError: Error {
    case feedsEmpty
    case unknown(String)
}

extension FeedsError: Errorable {
    var message: String {
        switch self {
        case .feedsEmpty:
            return Global.feedsEmpty.localized
        case .unknown(let message):
            return message
        }
    }
}
