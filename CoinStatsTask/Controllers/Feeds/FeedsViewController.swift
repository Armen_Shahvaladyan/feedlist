//
//  FeedsViewController.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import UIKit
import SKPhotoBrowser

class FeedsViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    //MARK: - Properties
    var router: FeedsRouter!
    var viewModel: FeedsViewModel!
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configureTable()
        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        navigationItem.title = Global.feeds.localized
        indicatorView.startAnimating()
        
        viewModel.syncFeeds { [weak self] error in
            guard let self = self else { return }
            self.indicatorView.stopAnimating()
            if let error = error {
                self.showAlert(with: error)
            } else {
                self.viewModel.fetchFeeds { error in
                    if let error = error {
                        self.showAlert(with: error)
                    } else {
                        self.showTableView()
                    }
                }
            }
        }
        
        viewModel.fetchFeeds { [weak self] (error) in
            guard let self = self else { return }
            if let error = error {
                switch error {
                case .unknown:
                    self.indicatorView.stopAnimating()
                    self.showAlert(with: error)
                default:
                    break
                }
            } else {
                self.indicatorView.stopAnimating()
                self.showTableView()
            }
        }
    }
    
    private func configureTable() {
        FeedCell.register(table: tableView)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = true
    }
    
    private func showTableView() {
        if tableView.isHidden {
            tableView.isHidden = false
        }
        tableView.reloadData()
    }
}

extension FeedsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.feeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FeedCell.cell(table: tableView, indexPath: indexPath)
        let feed = viewModel.feeds[indexPath.row]
        cell.configure(feed: feed, at: indexPath)
        cell.delegate = self
        return cell
    }
}

extension FeedsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let feed = viewModel.feeds[indexPath.row]
        viewModel.updateReadState([feed]) { _ in }
        if UIDevice.isIpad {
            if let splitViewController = splitViewController {
                router.perform(.feedDetails(feed), from: splitViewController)
            }
        } else {
            router.perform(.feedDetails(feed), from: self)
        }
        
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

extension FeedsViewController: FeedCellDelegate {
    func feedCell(_ cell: FeedCell, didToggle feed: Feed, at indexPath: IndexPath) {
        tableView.reloadData()
//        tableView.reloadRows(at: [indexPath], with: .automatic) TODO: Check it @Armen Shahvaladyan
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func feedCell(_ cell: FeedCell, didOpen gallery: [PhotoItem]?) {
        if let gallery = gallery {
            let images = gallery.map { SKPhoto.photoWithImageURL( $0.contentUrl) }
            router.perform(.gallery(images), from: self)
        }
    }
}

extension FeedsViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .feed
}
