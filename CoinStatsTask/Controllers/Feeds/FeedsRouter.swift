//
//  FeedsRouter.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import UIKit
import SKPhotoBrowser

enum FeedsSegue {
    case gallery([SKPhoto])
    case feedDetails(Feed)
}

protocol FeedsRoutable: Routable where SegueType == FeedsSegue, SourceType == UIViewController {

}

struct FeedsRouter: FeedsRoutable {
        
    func perform(_ segue: FeedsSegue, from source: UIViewController) {
        switch segue {
        case .feedDetails(let feed):
            let vc = FeedDetailsRouter.createFeedDetailsViewController(with: feed)
            if UIDevice.isIpad {
                let nc = UINavigationController(rootViewController: vc)
                (source as? UISplitViewController)?.showDetailViewController(nc, sender: nil)
            } else {
                source.navigationController?.pushViewController(vc, animated: true)
            }
        case .gallery(let images):
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(0)
            source.present(browser, animated: true)
        }
    }
}

extension FeedsRouter {
    static func createFeedsViewController(with feedService: FeedServiceProtocol) -> FeedsViewController {
        let vc = FeedsViewController.storyboardInstance
        vc.router = FeedsRouter()
        vc.viewModel = FeedsViewModel(feedService: feedService)
        
        return vc
    }
}
