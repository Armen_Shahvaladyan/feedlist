//
//  FeedCell.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit

protocol FeedCellDelegate: AnyObject {
    func feedCell(_ cell: FeedCell, didToggle feed: Feed, at indexPath: IndexPath)
    func feedCell(_ cell: FeedCell, didOpen gallery: [PhotoItem]?)
}

class FeedCell: TableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var galleryButton: BorderButton!
    @IBOutlet weak var showMoreButton: BorderButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var containerView: ShadowView!
    @IBOutlet weak var feedImageView: UIImageView!
    
    //MARK: - Proerties
    weak var delegate: FeedCellDelegate?
    private var feed: Feed!
    private var indexPath: IndexPath!
    
    //MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        galleryButton.setTitle(Action.gallery.localized, for: .normal)
        showMoreButton.setTitle(Action.showMore.localized, for: .normal)
        showMoreButton.setTitle(Action.lessMore.localized, for: .selected)
    }
    
    //MARK: - Public API
    func configure(feed: Feed, at indexPath: IndexPath) {
        self.feed = feed
        self.indexPath = indexPath
        
        titleLabel.attributedText = feed.attributedTitle
        dateLabel.attributedText = feed.attributedDate
        categoryLabel.attributedText = feed.attributedCategory
        bodyLabel.attributedText = feed.attributedBody
        feedImageView.setImage(path: feed.coverPhotoUrl, #imageLiteral(resourceName: "empty"))
        galleryButton.isHidden = feed.photoItems == nil || feed.photoItems?.isEmpty == true
        
        bodyLabel.isHidden = feed.body.isEmpty
        bodyLabel.numberOfLines = feed.isOpen ? 0 : 1
        showMoreButton.isSelected = feed.isOpen
        showMoreButton.isHidden = feed.body.isEmpty
        
        containerView.backgroundColor = feed.isRead ? .feedReadColor : .white
    }
    
    //MARK: - IBActions
    @IBAction func toggleBody(_ sender: UIButton) {
        feed.isOpen.toggle()
        sender.isSelected.toggle()
        bodyLabel.numberOfLines = feed.isOpen ? 0 : 1
        delegate?.feedCell(self, didToggle: feed, at: indexPath)
    }
    
    @IBAction func openGallery() {
        delegate?.feedCell(self, didOpen: feed.photoItems)
    }
}
