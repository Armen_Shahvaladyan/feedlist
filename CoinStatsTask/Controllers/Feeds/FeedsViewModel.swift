//
//  FeedsViewModel.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import Foundation

class FeedsViewModel {
    
    //MARK: - Properties
    private(set) var feeds: [Feed] = []
    private let feedService: FeedServiceProtocol
    
    //MARK: - Life Cycle
    init(feedService: FeedServiceProtocol) {
        self.feedService = feedService
    }
    
    //MARK: - Public API
    func syncFeeds(completion: @escaping (FeedsError?) -> Void) {
        feedService.syncFeeds { error in
            if let error = error {
                completion(error)
            } else {
                completion(nil)
            }
        }
    }
    
    func fetchFeeds(completion: @escaping (FeedsError?) -> Void) {
        feedService.fetchFeeds { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let feeds):
                if feeds.isEmpty {
                    completion(.feedsEmpty)
                } else {
                    self.feeds = feeds
                    completion(nil)
                }
            case .failure(let error):
                completion(.unknown(error.message))
            }
        }
    }
    
    func updateReadState(_ feeds: [Feed], completion: @escaping (FeedsError?) -> Void) {
        feeds.forEach { $0.isRead = true }
        feedService.updateFeeds(feeds, completion: completion)
    }
}
