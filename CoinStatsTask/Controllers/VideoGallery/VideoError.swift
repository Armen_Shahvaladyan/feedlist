//
//  VideoError.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 23.01.22.
//

import Foundation

enum VideoError: Error {
    case playError
}

extension VideoError: Errorable {
    var title: String {
        switch self {
        case .playError:
            return Global.somethingError.localized
        }
    }
    
    var message: String {
        switch self {
        case .playError:
            return Global.playError.localized
        }
    }
}
