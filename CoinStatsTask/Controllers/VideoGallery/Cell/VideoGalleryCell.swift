//
//  VideoGalleryCell.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 23.01.22.
//

import UIKit

class VideoGalleryCell: CollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: - Public API
    func configure(with video: VideoItem) {
        titleLabel.text = video.title
        imageView.setImage(path: video.thumbnailUrl, #imageLiteral(resourceName: "empty"))
    }
}
