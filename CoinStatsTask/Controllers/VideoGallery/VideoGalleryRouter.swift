//
//  VideoGalleryRouter.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit

enum VideoGallerySegue {
    case video(VideoItem)
}

protocol VideoGalleryRoutable: Routable where SegueType == VideoGallerySegue, SourceType == VideoGalleryViewController {

}

struct VideoGalleryRouter: VideoGalleryRoutable {
        
    func perform(_ segue: VideoGallerySegue, from source: VideoGalleryViewController) {
        switch segue {
        case .video(let video):
            if let error = playInYoutube(with: video.youtubeId) {
                source.showAlert(with: error)
            }
        }
    }
}

extension VideoGalleryRouter {
    static func createVideoGalleryViewController(with videos: [VideoItem]) -> VideoGalleryViewController {
        let vc = VideoGalleryViewController.storyboardInstance
        vc.router = VideoGalleryRouter()
        vc.viewModel = VideoGalleryViewModel(videos: videos)
        
        return vc
    }
    
    @discardableResult
    func playInYoutube(with youtubeId: String) -> VideoError? {
        if let youtubeURL = URL(string: "youtube://\(youtubeId)"),
            UIApplication.shared.canOpenURL(youtubeURL) {
            UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
            return nil
        } else if let youtubeURL = URL(string: "https://www.youtube.com/watch?v=\(youtubeId)") {
            UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
            return nil
        }
        
        return .playError
    }
}
