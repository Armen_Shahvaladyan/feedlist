//
//  VideoGalleryViewModel.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation

class VideoGalleryViewModel {
    
    //MARK: - Properties
    let videos: [VideoItem]
    
    //MARK: - Life Cycle
    init(videos: [VideoItem]) {
        self.videos = videos
    }
}
