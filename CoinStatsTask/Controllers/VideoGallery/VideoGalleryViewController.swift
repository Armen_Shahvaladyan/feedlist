//
//  VideoGalleryViewController.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit

class VideoGalleryViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Propertis
    var router: VideoGalleryRouter!
    var viewModel: VideoGalleryViewModel!
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //MARK: - Private API
    private func setup() {
        collectionView.delegate = self
        collectionView.dataSource = self
    }
}

extension VideoGalleryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = VideoGalleryCell.cell(collection: collectionView, indexPath: indexPath)
        let video = viewModel.videos[indexPath.item]
        cell.configure(with: video)
        return cell
    }
}

extension VideoGalleryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.bounds.size.width, height: 250)
    }
}

extension VideoGalleryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let video = viewModel.videos[indexPath.item]
        router.perform(.video(video), from: self)
    }
}

extension VideoGalleryViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .video
}
