//
//  FeedDetailsImageCell.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit

class FeedDetailsImageCell: CollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: - Public API
    func configure(with gallery: PhotoItem) {
        titleLabel.text = gallery.title
        imageView.setImage(path: gallery.contentUrl, #imageLiteral(resourceName: "empty"))
    }
}
