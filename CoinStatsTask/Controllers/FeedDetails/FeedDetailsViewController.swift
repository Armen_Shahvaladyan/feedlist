//
//  FeedDetailsViewController.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit
import SKPhotoBrowser

class FeedDetailsViewController: BaseViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var emptyImageView: UIImageView!
    @IBOutlet weak var imagePageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var router: FeedDetailsRouter!
    var viewModel: FeedDetailsViewModel!
    
    private lazy var videoButtonItem: UIBarButtonItem = {
        UIBarButtonItem(title: Action.video.localized,
                        style: .plain,
                        target: self,
                        action: #selector(openVideo))
    }()
    
    private lazy var galleryButtonItem: UIBarButtonItem = {
        UIBarButtonItem(title: Action.gallery.localized,
                        style: .plain,
                        target: self,
                        action: #selector(openGallery))
    }()
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        configureViews()
        configureNavigationItem()
    }
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { _ in
            self.collectionView.collectionViewLayout.invalidateLayout()
            self.collectionView?.reloadData()
        }, completion: nil)
    }
    
    //MARK: - Private API
    private func setup() {
        navigationItem.title = Global.feedDetails.localized
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func configureViews() {
        emptyImageView.setImage(path: viewModel.feed.coverPhotoUrl, #imageLiteral(resourceName: "empty"))
        emptyImageView.isHidden = !(viewModel.feed.photoItems == nil || viewModel.feed.photoItems?.isEmpty == true)
        titleLabel.attributedText = viewModel.feed.attributedTitle
        dateLabel.attributedText = viewModel.feed.attributedDate
        categoryLabel.attributedText = viewModel.feed.attributedCategory
        bodyLabel.attributedText = viewModel.feed.attributedBody
        bodyLabel.isHidden = viewModel.feed.body.isEmpty
        imagePageControl.numberOfPages = viewModel.numberOfPages
        imagePageControl.isHidden = viewModel.numberOfPages == 0
    }
    
    private func configureNavigationItem() {
        var barButtonItems = [UIBarButtonItem]()
        
        if viewModel.feed.videoItems != nil && viewModel.feed.videoItems?.isEmpty == false {
            barButtonItems.append(videoButtonItem)
            if viewModel.feed.photoItems != nil && viewModel.feed.photoItems?.isEmpty == false {
                barButtonItems.append(galleryButtonItem)
            }
        } else if viewModel.feed.photoItems != nil && viewModel.feed.photoItems?.isEmpty == false {
            barButtonItems.append(galleryButtonItem)
        }
        
        navigationItem.rightBarButtonItems = barButtonItems
    }
    
    private func goToGallery(at index: Int) {
        if let photoItems = viewModel.feed.photoItems {
            let images = photoItems.map { SKPhoto.photoWithImageURL( $0.contentUrl) }
            let didScrollImage: (Int) -> Void = { [weak self] (index) in
                self?.changeImage(at: index)
            }
            router.perform(.gallery(images, index, didScrollImage), from: self)
        }
    }
    
    @objc private func openGallery() {
        if let photoItems = viewModel.feed.photoItems {
            let images = photoItems.map { SKPhoto.photoWithImageURL( $0.contentUrl) }
            let didScrollImage: (Int) -> Void = { [weak self] (index) in
                self?.changeImage(at: index)
            }
            router.perform(.gallery(images, 0, didScrollImage), from: self)
        }
    }
    
    @objc private func openVideo() {
        if let videos = viewModel.feed.videoItems {
            router.perform(.video(videos), from: self)
        }
    }
    
    private func changeImage(at index: Int) {
        imagePageControl.currentPage = index
        collectionView.scrollToItem(at: IndexPath(item: index, section: 0),
                                    at: .centeredHorizontally,
                                    animated: true)
    }
    
    //MARK: - IBActions
    @IBAction func changePage(_ sender: UIPageControl) {
        changeImage(at: sender.currentPage)
    }
}

extension FeedDetailsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let gallery = viewModel.feed.photoItems {
            return gallery.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = FeedDetailsImageCell.cell(collection: collectionView, indexPath: indexPath)
        
        if let photoItems = viewModel.feed.photoItems {
            cell.configure(with: photoItems[indexPath.item])
        }
        return cell
    }
}

extension FeedDetailsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView.bounds.size
    }
}

extension FeedDetailsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        goToGallery(at: indexPath.item)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
        imagePageControl.currentPage = Int(currentPage)
    }
}

extension FeedDetailsViewController: StoryboardInstance {
    static var storyboardName: StoryboardName = .feed
}
