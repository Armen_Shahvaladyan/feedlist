//
//  FeedDetailsRouter.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit
import SKPhotoBrowser

enum FeedDetailsSegue {
    case gallery([SKPhoto], Int, (Int) -> Void)
    case video([VideoItem])
}

protocol FeedDetailsRoutable: Routable where SegueType == FeedDetailsSegue, SourceType == FeedDetailsViewController {

}

class FeedDetailsRouter: FeedDetailsRoutable {
    
    var didScrollImage: ((Int) -> Void)?
    
    func perform(_ segue: FeedDetailsSegue, from source: FeedDetailsViewController) {
        switch segue {
        case let .gallery(images, index, didScrollImage):
            self.didScrollImage = didScrollImage
            let browser = SKPhotoBrowser(photos: images)
            browser.delegate = self
            browser.initializePageIndex(index)
            source.present(browser, animated: true)
        case .video(let videos):
            let vc = VideoGalleryRouter.createVideoGalleryViewController(with: videos)
            source.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension FeedDetailsRouter {
    static func createFeedDetailsViewController(with feed: Feed) -> FeedDetailsViewController {
        let vc = FeedDetailsViewController.storyboardInstance
        vc.router = FeedDetailsRouter()
        vc.viewModel = FeedDetailsViewModel(feed: feed)
        
        return vc
    }
}

extension FeedDetailsRouter: SKPhotoBrowserDelegate {
    func didScrollToIndex(_ browser: SKPhotoBrowser, index: Int) {
        didScrollImage?(index)
    }
}
