//
//  FeedDetailsViewModel.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation

class FeedDetailsViewModel {
    
    //MARK: - Properties
    let feed: Feed
    
    var numberOfPages: Int {
        feed.photoItems?.count ?? 0
    }
    
    //MARK: - Life Cycle
    init(feed: Feed) {
        self.feed = feed
    }
}
