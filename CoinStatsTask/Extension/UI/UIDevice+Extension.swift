//
//  UIDevice+Extension.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 24.01.22.
//

import UIKit

extension UIDevice {
    static var isIpad: Bool {
        return current.userInterfaceIdiom == .pad
    }
    
    static var isIphone: Bool {
        return !isIpad
    }
}
