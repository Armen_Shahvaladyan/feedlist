//
//  UIViewController+Alert.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit

extension UIViewController {
    func showAlert(with error: Errorable) {
        let ac = UIAlertController(title: error.title,
                                   message: error.message,
                                   preferredStyle: .alert)
        
        let ok = UIAlertAction(title: Action.ok.localized, style: .default)
        
        ac.addAction(ok)
        ac.preferredAction = ok
        
        self.present(ac, animated: true, completion:nil)
    }
}
