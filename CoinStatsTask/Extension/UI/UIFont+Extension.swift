//
//  UIFont+Extension.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import UIKit

extension UIFont {
    enum Font: String {
        case text = "SFUIText"
        case display = "SFUIDisplay"
    }
   
    class func SF(_ font: Font, weight: UIFont.Weight, size: CGFloat) -> UIFont {
        let adjustedFont = UIFont.systemFont(ofSize: size, weight: weight)
        if font == .display {
            return UIFontMetrics(forTextStyle: .caption2).scaledFont(for: adjustedFont)
        } else {
            return UIFontMetrics(forTextStyle: .title3).scaledFont(for: adjustedFont)
        }
    }
}
