//
//  UILabel+Extension.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit

extension UILabel {
    var isTruncated: Bool {
        layoutIfNeeded()
        
        let rectBounds = CGSize(width: bounds.width, height: .greatestFiniteMagnitude)
        var fullTextHeight: CGFloat?
        
        if attributedText != nil {
            fullTextHeight = attributedText?.boundingRect(with: rectBounds,
                                                          options: .usesLineFragmentOrigin,
                                                          context: nil).size.height
        } else {
            fullTextHeight = text?.boundingRect(with: rectBounds,
                                                options: .usesLineFragmentOrigin,
                                                attributes: [.font: font!], context: nil).size.height
        }
        
        return (fullTextHeight ?? 0) > bounds.size.height
    }
}
