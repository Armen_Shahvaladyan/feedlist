//
//  UISplitViewController+Extension.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 24.01.22.
//

import UIKit

extension UISplitViewController {
    var primaryViewController: UIViewController? {
        if let nc = viewControllers.last as? UINavigationController {
            return nc.viewControllers.first
        }
        return viewControllers.first
    }

    var secondaryViewController: UIViewController? {
        if let nc = viewControllers.last as? UINavigationController {
            return nc.viewControllers.first
        }
        return viewControllers.last
    }
}
