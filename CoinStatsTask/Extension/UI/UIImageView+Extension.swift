//
//  UIImageView+Extension.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit
import SDWebImage

extension UIImageView {
    func setImage(path: String?, _ placeHolder: UIImage?) {
        if let path = path, !path.isEmpty {
            sd_imageIndicator = SDWebImageActivityIndicator.gray
            sd_setImage(with: URL(string: path), placeholderImage: nil, options: []) { (image, error, cacheType, url) in
                if error == nil {
                    self.sd_imageIndicator = .none
                    self.image = image
                } else {
                    self.image = placeHolder
                }
            }
        } else {
            image = placeHolder
        }
    }
}
