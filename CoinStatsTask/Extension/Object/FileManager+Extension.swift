//
//  FileManager+Extension.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation

extension FileManager {
    
    private static func libraryAppDir() -> URL {
        let libraryPath: String = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true).first!
        return URL(fileURLWithPath: libraryPath).appendingPathComponent(Bundle.main.bundleIdentifier!, isDirectory: true)
    }
    
    private static func databaseDir() -> URL {
        return FileManager.libraryAppDir().appendingPathComponent("Database", isDirectory: true)
    }
    
    static func feedDatabaseURL() -> URL {
        return FileManager.databaseDir().appendingPathComponent("FeedData.sqlite")
    }
}
