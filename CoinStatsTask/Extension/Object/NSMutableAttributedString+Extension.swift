//
//  NSMutableAttributedString+Extension.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit

extension NSMutableAttributedString {
    convenience init(fullString: String,
                     fullStringColor: UIColor,
                     fullStringFont: UIFont,
                     subString: String,
                     subStringColor: UIColor,
                     subStringFont: UIFont,
                     underline: Bool = false) {
        let rangeOfSubString = (fullString as NSString).range(of: subString)
        let rangeOfFullString = NSRange(location: 0, length: fullString.count)
        let attributedString = NSMutableAttributedString(string:fullString)
        attributedString.addAttribute(.font, value: fullStringFont, range: rangeOfFullString)
        attributedString.addAttribute(.foregroundColor, value: fullStringColor, range: rangeOfFullString)
        attributedString.addAttribute(.font, value: subStringFont, range: rangeOfSubString)
        attributedString.addAttribute(.foregroundColor, value: subStringColor, range: rangeOfSubString)
        if underline {
            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: rangeOfSubString)
        }
        self.init(attributedString: attributedString)
    }
}
