//
//  Errorable.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation

protocol Errorable: Error {
    var title: String { get }
    var message: String { get }
}

extension Errorable {
    var title: String {
        return Global.attention.localized
    }
}
