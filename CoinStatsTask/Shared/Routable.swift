//
//  Routable.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import Foundation

protocol Routable {
    associatedtype SegueType
    associatedtype SourceType
    func perform(_ segue: SegueType, from source: SourceType)
}
