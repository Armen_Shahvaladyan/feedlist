//
//  Launcher.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import UIKit

struct Launcher {
    enum RootScreen {
        case feeds(FeedServiceProtocol)
    }
    
    static func perform(segue: RootScreen, window: UIWindow) {
        switch segue {
        case .feeds(let feedService):
            if UIDevice.isIpad {
                let split = UISplitViewController()
                let feeds = FeedsRouter.createFeedsViewController(with: feedService)
                split.viewControllers = [NavigationController(rootViewController: feeds),
                                         NavigationController(rootViewController: UIViewController())]
                window.rootViewController = split
            } else {
                let vc = FeedsRouter.createFeedsViewController(with: feedService)
                let navigationController = NavigationController(rootViewController: vc)
                window.rootViewController = navigationController
            }
            window.makeKeyAndVisible()
        }
    }
}
