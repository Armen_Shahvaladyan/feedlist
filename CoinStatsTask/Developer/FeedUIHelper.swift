//
//  FeedHelper.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation
import UIKit

final class FeedUIHelper {
    
    //MARK: - Properties
    private static let font = UIFont.SF(.display, weight: .bold, size: 17)
    private static let subFont = UIFont.SF(.display, weight: .medium, size: 17)
    
    //MARK: - Public API
    static func date(with feed: Feed) -> NSMutableAttributedString {
        let dateformat = DateFormatter()
        dateformat.dateFormat = "d MMM, yyyy HH:mm"
        let dateString = dateformat.string(from: feed.date)
        let fullText = Global.date.localized + ": " + dateString
        let attributedString = NSMutableAttributedString(fullString: fullText,
                                                         fullStringColor: .black,
                                                         fullStringFont: font,
                                                         subString: Global.date.localized + ":",
                                                         subStringColor: .appDarkOrange,
                                                         subStringFont: subFont)
        return attributedString
    }
    
    static func title(with feed: Feed) -> NSMutableAttributedString {
        let fullText = Global.title.localized + ": " + feed.title
        let attributedString = NSMutableAttributedString(fullString: fullText,
                                                         fullStringColor: .black,
                                                         fullStringFont: font,
                                                         subString: Global.title.localized + ":",
                                                         subStringColor: .appDarkOrange,
                                                         subStringFont: subFont)
        return attributedString
    }
    
    static func category(with feed: Feed) -> NSMutableAttributedString {
        let fullText = Global.category.localized + ": " + feed.category
        let attributedString = NSMutableAttributedString(fullString: fullText,
                                                         fullStringColor: .black,
                                                         fullStringFont: font,
                                                         subString: Global.category.localized + ":",
                                                         subStringColor: .appDarkOrange,
                                                         subStringFont: subFont)
        return attributedString
    }
    
    static func body(with feed: Feed) -> NSMutableAttributedString {
        let fullText = Global.body.localized + ": " + feed.body
        let attributedString = NSMutableAttributedString(fullString: fullText,
                                                         fullStringColor: .black,
                                                         fullStringFont: font,
                                                         subString: Global.body.localized + ":",
                                                         subStringColor: .appDarkOrange,
                                                         subStringFont: subFont)
        return attributedString
    }
}
