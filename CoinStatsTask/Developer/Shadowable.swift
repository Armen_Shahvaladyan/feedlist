//
//  Shadowable.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit

protocol Shadowable {
    var cornerRadius: CGFloat { get set }
    var shadowRadius: CGFloat { get set }
    var shadowOpacity: Float { get set }
    var shadowOffset: CGSize { get set }
    var shadowColor: UIColor? { get set }
}

protocol Bordering {
    var borderWidth: CGFloat { get set }
    var borderColor: UIColor? { get set }
}
