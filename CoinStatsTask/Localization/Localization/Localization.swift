//
//  Localization.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import UIKit

fileprivate let appleLanguagesKey = "AppleLanguages"

enum Language: String, Hashable {
    case en = "en"
    case ru = "ru"
    case uk = "uk"
    
    public var hashValue: Int { get { return rawValue.hashValue } }
    
    //language code as stored in server
    func serverCode() -> String {
        switch self {
        case .en:
            return "en"
        case .ru:
            return "ru"
        case .uk:
            return "uk"
        }
    }
    
    func localizedDescription() -> String {
        switch self {
        case .en:
            return "English"
        case .ru:
            return "Русский"
        case .uk:
            return "Украинский"
        }
    }
    
    func localeIdentifier() -> String {
        switch self {
        case .en: return "en_US_POSIX"
        case .ru: return "ru_RU"
        case .uk: return "ru_RU"
        }
    }
}

class Localization {
    static let `default` = Localization()
    
    var language: Language {
        get {
            if let languageCode = UserDefaults.standard.string(forKey: appleLanguagesKey),
               let language = Language(rawValue: languageCode) {
                return language
            } else {
                guard let preferredLanguage = Locale.preferredLanguages.first,
                      let localization = Language(rawValue: preferredLanguage.substring(to: 2)) else {
                    return Language.en
                }
                if localization == .uk {
                    return .ru
                }
                return localization
            }
        }
    }
    
    
    private init() { }
    
    func getLanguage() -> String {
        let appleLanguages = UserDefaults.standard.object(forKey: "AppleLanguages") as! [String]
        let prefferedLanguage = appleLanguages[0]
        if prefferedLanguage.contains("-") {
            let array = prefferedLanguage.components(separatedBy: "-")
            return array[0]
        }
        return prefferedLanguage
    }
}

extension NSNotification.Name {
    public static let LocalizationDidChange = NSNotification.Name("LocalizationDidChange")
}

extension String {
    func localized(tableName: String) -> String {
        let bundle = Bundle.main.path(forResource: Localization.default.language.rawValue, ofType: "lproj")!
        return NSLocalizedString(self, tableName: tableName, bundle: Bundle(path: bundle)!, value: "", comment: "")
    }
}

postfix operator ~
postfix func ~ (string: String) -> String {
    return NSLocalizedString(string, comment: "")
}

extension Bundle {
    static func swizzleLocalization() {
        let orginalSelector = #selector(localizedString(forKey:value:table:))
        guard let orginalMethod = class_getInstanceMethod(self, orginalSelector) else { return }

        let mySelector = #selector(myLocaLizedString(forKey:value:table:))
        guard let myMethod = class_getInstanceMethod(self, mySelector) else { return }

        if class_addMethod(self, orginalSelector, method_getImplementation(myMethod), method_getTypeEncoding(myMethod)) {
            class_replaceMethod(self, mySelector, method_getImplementation(orginalMethod), method_getTypeEncoding(orginalMethod))
        } else {
            method_exchangeImplementations(orginalMethod, myMethod)
        }
    }

    @objc private func myLocaLizedString(forKey key: String,value: String?, table: String?) -> String {
        guard let bundlePath = Bundle.main.path(forResource: Localization.default.language.rawValue, ofType: "lproj"),
            let bundle = Bundle(path: bundlePath) else {
                return Bundle.main.myLocaLizedString(forKey: key, value: value, table: table)
        }
        return bundle.myLocaLizedString(forKey: key, value: value, table: table)
    }
}
