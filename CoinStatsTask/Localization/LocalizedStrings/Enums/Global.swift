//
//  Dialog.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation

enum Global: String, Localizable {
    case feedsEmpty
    case attention
    case date
    case title
    case category
    case body
    case feedDetails
    case feeds
    case playError
    case somethingError
    
    var tableName: String { return "GlobalStrings" }
}
