//
//  Action.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 22.01.22.
//

import Foundation

enum Action: String, Localizable {
    case ok
    case showMore
    case lessMore
    case gallery
    case video
    
    var tableName: String { return "ActionElementStrings" }
}
