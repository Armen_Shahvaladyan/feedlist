//
//  StoryboardName.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 21.01.22.
//

import Foundation

enum StoryboardName: String {
    case feed  = "Feed"
    case video = "Video"
}
