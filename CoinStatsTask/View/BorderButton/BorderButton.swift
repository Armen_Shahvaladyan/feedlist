//
//  BorderButton.swift
//  CoinStatsTask
//
//  Created by Armen Shahvaladyan on 23.01.22.
//

import UIKit

class BorderButton: UIButton {
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 6
        layer.masksToBounds = true
    }
    
    //MARK: - Private API
    private func setup() {
        tintColor = .clear
        backgroundColor = .appOrange
        contentHorizontalAlignment = .center
        setTitleColor(.black, for: .normal)
        setTitleColor(.black, for: .selected)
        titleLabel?.font = UIFont.SF(.text, weight: .medium, size: 17)
    }
}
